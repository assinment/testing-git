import ProfileHeaders from "../components/profile/profileHeader";
import ProfileActions from "../components/profile/profileActions";
import ProfileHistory from "../components/profile/profilehistory";

const Profile = () => {


    const [user] = useUser()

    return (
        <>
            <h1>Profile</h1>
            <ProfileHeader username={user.username}/>
            <ProfileActions/>
            <ProfileHistory orders={user.orders}/>
        </>




    )
}
export default Profile