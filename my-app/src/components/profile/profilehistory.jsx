import ProfileOrdersItem from "./ProfileOrdersItem";

const ProfileHistory = ({orders}) => {
    const orderList = orders.map((order, index) => <ProfileOrdersItem key={index + "-" + order} order={order}/>)

    return (
        <section>
            <h4>Your order history</h4>
            <ul>
                {orderList}
            </ul>
        </section>

    )
}
export default ProfileHistory;