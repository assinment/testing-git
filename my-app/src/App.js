

import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Login from "./views/Login";
import Orders from "./views/Orders";
import Profile from "./views/Profile";
// ji

function App() {
  return (
      <BrowserRouter>
        <div className="App">
          <Routes>
            <Route path="/" element={<Login/>}/>
            <Route path="/Orders" element={<Orders/>} />
            <Route path="Profile" element={<Profile/>} />
          </Routes>
        </div>
  </BrowserRouter>
    );
}
// hi
export default App;
